<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('xtimbra')->group(base_path('routes/xtimbra.php'));
