<?php
use Illuminate\Support\Facades\Route;

Route::post('timbres', 'Zamgod@timbres');
Route::get('pruebas', 'Zamgod@pruebas');
Route::post('timbrarXml', 'Zamgod@timbrarXml');
Route::post('cancelarXml', 'Zamgod@cancelarXml');
