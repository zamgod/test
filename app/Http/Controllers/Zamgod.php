<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use SoapClient;

class Zamgod extends Controller
{
  private $url;
  private $codigo_sucursal;
  private $cuenta;

  function __construct()
  {
    $this->url = 'http://xtimbra.sifel.com.mx/ServiceXTimbra.php?wsdl';
    $this->codigo_sucursal = 'main';
  }

  function pruebas()
  {
    dd('conectado');
  }

  function timbres(Request $request)
  {
    $this->cuenta = $request->get('cuenta');
    $this->url = $request->get('url') ? $request->get('url') : 'http://xtimbra.sifel.com.mx/ServiceXTimbra.php?wsdl';
    return $this->ConsultaPaquetesTimbre($request);
  }

  function timbrarXml(Request $request)
  {
    $this->codigo_sucursal = $request->get('sucursal') ? $request->get('sucursal') : 'main';
    $this->cuenta = $request->get('cuenta');
    $this->url = $request->get('url') ? $request->get('url') : 'http://xtimbra.sifel.com.mx/ServiceXTimbra.php?wsdl';
    return $this->Timbrar($request);

  }

  function cancelarXml(Request $request)
  {
    $this->codigo_sucursal = $request->get('sucursal') ? $request->get('sucursal') : 'main';
    $this->cuenta = $request->get('cuenta');
    $this->url = $request->get('url') ? $request->get('url') : 'http://xtimbra.sifel.com.mx/ServiceXTimbra.php?wsdl';
    return $this->Cancelar($request);
  }

  private function ConsultaPaquetesTimbre(Request $request)
  {
    $paginacion = [
      'Numero_Pagina' => 1,
      'Registros_PorPagina' => 30
    ];
    $client = new SoapClient($this->url);
    $DatosSolicitud = array('Cuenta' => $this->cuenta, 'Paginacion' => $paginacion);
    $Solicitud = array($DatosSolicitud);
    $res = $client->__soapCall('Consulta_PaquetesTimbre', $Solicitud);
    $res->request = $request->all();
    return $res;
  }

  private function Timbrar(Request $request)
  {
    $base64Xml = base64_encode($request->get('xml'));
    $client = new SoapClient($this->url);
    $DatosSolicitud = array('Cuenta' => $this->cuenta, 'Codigo_Sucursal' => $this->codigo_sucursal, "XML" => $base64Xml);
    $Solicitud = array($DatosSolicitud);
    $res = $client->__soapCall('Timbrar', $Solicitud);
    $res->request = $request->all();
    return $res;
  }

  private function Cancelar(Request $request)
  {
    $client = new SoapClient($this->url);
    $DatosSolicitud = [
      'Cuenta' => $this->cuenta,
      'Codigo_Sucursal' => $this->codigo_sucursal,
      'UUID' => $request->get('UUID'),
      'FolioSustitucion' => '',
      'Motivo' => '02'
    ];
    $Solicitud = array($DatosSolicitud);
    $res = $client->__soapCall('Cancelar', $Solicitud);
    $res->request = $request->all();
    return $res;
  }

}
